<?php
	class application_controller extends Banshee\controller {
		private $url = array("url" => "overview");

		private function show_overview() {
			if (($applications = $this->model->get_applications()) === false) {
				$this->view->add_tag("result", $this->view->global_text("error_database"), $this->url);
				return;
			}

			$this->view->open_tag("overview");
			foreach ($applications as $application) {
				$application["location"] = LOCATION[$application["location"]];
				$this->view->record($application, "application");
			}
			$this->view->close_tag();
		}

		private function show_application($application_id) {
			if (($application = $this->model->get_application($application_id)) == false) {
				$this->view->add_tag("result", "Application not found.", $this->url);
				return;
			}

			if (($information = $this->model->get_information($application_id)) === false) {
				$this->view->add_tag("result", "Error while retrieving information.", $this->url);
				return;
			}

			if (($connections = $this->model->get_connections($application_id)) === false) {
				$this->view->add_tag("result", "Error while retrieving connections.", $this->url);
				return;
			}

			if (($file_operations = $this->model->get_file_operations($application_id)) === false) {
				$this->view->add_tag("result", "Error while retrieving file operations.", $this->url);
				return;
			}

			if (($used_by = $this->model->get_used_by($application_id)) === false) {
				$this->view->add_tag("result", "Error while retrieving application usage.", $this->url);
				return;
			}

			if (($runs_at = $this->model->get_runs_at($application_id)) === false) {
				$this->view->add_tag("result", "Error while retrieving used hardware.", $this->url);
				return;
			}

			$this->view->add_help_button();

			$this->view->add_javascript('banshee/jquery.windowframe.js');
			$this->view->add_javascript("dialog.js");

			$this->view->title = $application["name"];

			$this->view->open_tag("application", array("id" => $application_id, "previous" => $this->page->previous));

			$application["confidentiality"] = CONFIDENTIALITY[$application["confidentiality"]] ?? "";
			$application["integrity"] = INTEGRITY[$application["integrity"]] ?? "";
			$application["availability"] = AVAILABILITY[$application["availability"]] ?? "";

			$application["location"] = LOCATION[$application["location"]];
			$application["internet"] = $this->language->global_text(show_boolean($application["internet"]));
			$this->view->record($application);

			$this->view->open_tag("labels");
			foreach ($application["labels"] as $label) {
				$this->view->add_tag("label", $label["name"], array(
					"id"  => $label["id"],
					"cid" => $label["category_id"]));
			}
			$this->view->close_tag();

			$this->view->open_tag("information");
			foreach ($information as $info) {
				$info["type"] = INFORMATION_STORAGE_TYPES[$info["type"]];
				$this->view->record($info, "information");
			}
			$this->view->close_tag();

			$this->view->open_tag("connections");
			foreach ($connections as $connection) {
				$connection["data_flow"] = CONNECTION_DATA_FLOW[$connection["data_flow"]];
				$this->view->record($connection, "connection");
			}
			$this->view->close_tag();

			$this->view->open_tag("file_operations");
			foreach ($file_operations as $file_operation) {
				$file_operation["data_flow"] = FILE_DATA_FLOW[$file_operation["data_flow"]];
				$this->view->record($file_operation, "file_operation");
			}
			$this->view->close_tag();

			$this->view->open_tag("used_by");
			foreach ($used_by as $entity) {
				$this->view->record($entity, "entity");
			}
			$this->view->close_tag();

			$this->view->open_tag("runs_at");
			foreach ($runs_at as $device) {
				$this->view->record($device, "device");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = $this->language->global_text("applications");
			$this->view->add_css("includes/print.css");

			if ($this->page->parameter_numeric(0)) {
				$this->show_application((int)$this->page->parameters[0]);
			} else {
				$this->show_overview();
			}
		}
	}
?>
