<?php
	class processing_controller extends Banshee\controller {
		private function show_processings() {
			if (($processings = $this->model->get_processings()) === false) {
				$this->view->add_tag("result", $this->view->global_text("error_database"));
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("processings");
			foreach ($processings as $processing) {
				$processing["role"] = PROCESSING_ROLES[$processing["role"]];
				$this->view->record($processing, "processing");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_processing($processing) {
			$this->view->title = $processing["description"];

			$this->view->open_tag("processing", array("previous" => $this->page->previous));

			$processing["role"] = PROCESSING_ROLES[$processing["role"]];
			$this->view->record($processing);

			$this->view->open_tag("applications");
			foreach ($processing["applications"] as $application) {
				$application["type"] = INFORMATION_STORAGE_TYPES[$application["type"]];
				$this->view->record($application, "application");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function export_processing() {
			if (($processings = $this->model->get_processings()) === false) {
				$this->view->add_tag("result", $this->view->global_text("error_database"));
				return;
			}

			$csv = new Banshee\csvfile();

			$csv->add_line(array("Processing activities as a controller"));
			$csv->add_line(array("Description of the processing",
			                     "Contact information of the controller",
			                     "Purpose of the processing",
			                     "Categories of the data subjects and personal data",
			                     "Categories of recipients",
			                     "Data transfer to third country or international organisation",
			                     "Envisaged time limits for data erasure",
			                     "Technical and organisational security measures"));
			foreach ($processings as $p) {
				if ($p["role"] != 0) {
					continue;
				}

				$line = array($p["description"], $p["contact"], $p["purpose"], $p["subject"],
				               $p["recipient"], $p["transfer"], $p["erasure"], $p["security"]);
				$csv->add_line($line);
			}

			$csv->add_line(array());

			$csv->add_line(array("Processing activities as a processor"));
			$csv->add_line(array("Description of the processing",
			                     "Contact information of the processor and responsible controller",
			                     "Categories of the processing carried out on behalf of controller",
			                     "Data transfer to third country or international organisation",
			                     "Technical and organisational security measures"));
			foreach ($processings as $p) {
				if ($p["role"] != 1) {
					continue;
				}

				$line = array($p["description"], $p["contact"], $p["purpose"], $p["transfer"], $p["security"]);
				$csv->add_line($line);
			}

			$csv->add_to_view($this->view, "processings");
		}

		public function execute() {
			$this->view->title = $this->language->module_text("processing");
			$this->view->add_css("includes/print.css");

			if ($this->view->mode == "csv") {
				$this->export_processing();
			}

			$this->view->add_help_button();

			if ($this->page->parameter_numeric(0) == false) {
				$this->show_processings();
			} else if (($processing = $this->model->get_processing($this->page->parameters[0])) == false) {
				$this->view->add_tag("result", "Processing not found.");
			} else {
				$this->show_processing($processing);
			}
		}
	}
?>
