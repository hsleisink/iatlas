<?php
	class fileoperation_controller extends Banshee\controller {
		private $url = array("url" => "overview");

		private function show_file_operation($id) {
			if (($file_operation = $this->model->get_file_operation($id)) == false) {
				$this->view->add_tag("result", "File operation not found.", $this->url);
				return;
			}

			$file_operation["data_flow"] = FILE_DATA_FLOW[$file_operation["data_flow"]];
			$this->view->record($file_operation, "file_operation", array("previous" => $this->page->previous));
		}

		public function execute() {
			if ($this->page->parameter_numeric(0)) {
				$this->show_file_operation($this->page->parameters[0]);
			} else {
				$this->view->add_tag("result", "No file operation specified.", $this->url);
			}
		}
	}
?>
