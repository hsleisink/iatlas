<?php
	class information_controller extends Banshee\controller {
		private $url = array("url" => "overview");

		private function show_overview() {
			if (($information = $this->model->get_all_information()) === false) {
				$this->view->add_tag("result", $this->view->global_text("error_database"), $this->url);
				return false;
			}

			$this->view->open_tag("overview");
			foreach ($information as $info) {
				$this->view->record($info, "information");
			}
			$this->view->close_tag();
		}

		private function show_information($information_id) {
			if (($information = $this->model->get_information($information_id)) === false) {
				$this->view->add_tag("result", "Data collection not found.", $this->url);
				return false;
			}

			if (($applications = $this->model->get_applications($information_id)) === false) {
				$this->view->add_tag("result", "Error retrieving applications.", $this->url);
				return false;
			}

			if (($processings = $this->model->get_processings($information_id)) === false) {
				$this->view->add_tag("result", "Error retrieving processings.", $this->url);
				return false;
			}

			$this->view->add_help_button();

			$this->view->add_javascript('banshee/jquery.windowframe.js');
			$this->view->add_javascript("dialog.js");

			$this->view->title = $information["name"];

			$this->view->open_tag("information", array("id" => $information_id, "previous" => $this->page->previous));

			$information["confidentiality"] = CONFIDENTIALITY[$information["confidentiality"]];
			$information["integrity"] = INTEGRITY[$information["integrity"]];
			$information["availability"] = AVAILABILITY[$information["availability"]];
			$this->view->record($information);

			$this->view->open_tag("applications");
			foreach ($applications as $application) {
				$application["type"] = INFORMATION_STORAGE_TYPES[$application["type"]];
				$this->view->record($application, "application");
			}
			$this->view->close_tag();

			$this->view->open_tag("processings");
			foreach ($processings as $processing) {
				$processing["role"] = PROCESSING_ROLES[$processing["role"]];
				$this->view->record($processing, "processing");
			}
			$this->view->close_tag();

			$this->view->open_tag("labels");
			foreach ($information["labels"] as $label) {
				$this->view->add_tag("label", $label["name"], array(
					"id"  => $label["id"],
					"cid" => $label["category_id"]));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = $this->language->global_text("information");
			$this->view->add_css("includes/print.css");

			if ($this->page->parameter_numeric(0)) {
				$this->show_information((int)$this->page->parameters[0]);
			} else {
				$this->show_overview();
			}
		}
	}
?>
