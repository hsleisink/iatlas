<?php
	class cms_translation_controller extends Banshee\controller {
		const GLOBAL_MODULE = "*";

		private $modules = array();

		private function show_overview() {
			if (($translations = $this->model->get_translations($_SESSION["translation_module"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("modules", array("current" => $_SESSION["translation_module"]));
			foreach ($this->modules as $module) {
				$this->view->add_tag("module", $module);
			}
			$this->view->close_tag();

			$this->view->open_tag("translations");
			foreach ($translations as $translation) {
				$lines = explode("\n", $translation[$this->view->language]);
				$text = strip_tags(trim($lines[0]));
				if ($text == "") {
					$text = strip_tags(trim($lines[1] ?? ""));
				}
				$translation["text"] = $text;

				$translation["javascript"] = show_boolean($translation["javascript"]);

				$this->view->record($translation, "translation");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_translation_form($translation) {
			$this->view->open_tag("edit");

			$translation["javascript"] = show_boolean($translation["javascript"] ?? false);
			$this->view->record($translation, "translation");

			$this->view->open_tag("modules", array("current" => $_SESSION["translation_module"]));
			foreach ($this->modules as $module) {
				$this->view->add_tag("module", $module);
			}
			$this->view->close_tag();

			$this->view->open_tag("languages");
			foreach ($this->language->supported as $code => $language) {
				$this->view->add_tag("language", $language, array("code" => $code));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$modules = page_to_module(array_merge(config_file("public_modules"), config_file("private_modules")));
			array_unshift($modules, ERROR_MODULE);
			array_unshift($modules, LOGIN_MODULE);
			foreach ($modules as $key => $module) {
				if (substr($module, 0, 3) == "cms") {
					unset($modules[$key]);
				}
			}
			sort($modules);
			array_unshift($modules, self::GLOBAL_MODULE);
			$this->modules = $modules;

			if (isset($_SESSION["translation_module"]) == false) {
				$_SESSION["translation_module"] = self::GLOBAL_MODULE;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save translation") {
					/* Save translation
					 */
					if ($this->model->save_okay($_POST) == false) {
						$this->show_translation_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create translation
						 */
						if ($this->model->create_translation($_POST) === false) {
							$this->view->add_message("Error creating translation.");
							$this->show_translation_form($_POST);
						} else {
							$_SESSION["translation_module"] = $_POST["module"];
							$this->user->log_action("translation %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update translation
						 */
						if ($this->model->update_translation($_POST) === false) {
							$this->view->add_message("Error updating translation.");
							$this->show_translation_form($_POST);
						} else {
							$this->user->log_action("translation %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete translation") {
					/* Delete translation
					 */
					if ($this->model->delete_translation($_POST["id"]) === false) {
						$this->view->add_message("Error deleting translation.");
						$this->show_translation_form($_POST);
					} else {
						$this->user->log_action("translation %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "module") {
					/* Change module
					 */
					$_SESSION["translation_module"] = $_POST["module"];
					$this->show_overview();
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New translation
				 */
				$translation = array("module" => $_SESSION["translation_module"]);
				$this->show_translation_form($translation);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit translation
				 */
				if (($translation = $this->model->get_translation($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", "translation not found.");
				} else {
					$this->show_translation_form($translation);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
