<?php
	class cms_information_controller extends Banshee\controller {
		private function show_overview() {
			if (($_SESSION["information_search"] ?? "") == "") {
				if (($information_count = $this->model->count_information()) === false) {
					$this->view->add_tag("result", "Database error.");
					return;
				}

				$paging = new Banshee\pagination($this->view, "information", $this->settings->admin_page_size, $information_count);

				if (($information = $this->model->get_all_information($paging->offset, $paging->size)) === false) {
					$this->view->add_tag("result", "Database error.");
					return;
				}
			} else {
				if (($information = $this->model->get_all_information()) === false) {
					$this->view->add_tag("result", "Database error.");
					return;
				}
			}

			$this->view->open_tag("overview", array("search" => $_SESSION["information_search"] ?? ""));

			$this->view->open_tag("information");
			foreach ($information as $info) {
				$this->view->record($info, "information");
			}
			$this->view->close_tag();

			if (($_SESSION["information_search"] ?? "") == "") {
				$paging->show_browse_links();
			}

			$this->view->close_tag();
		}

		private function add_list($label, $data) {
			$this->view->open_tag($label);
			foreach ($data as $text) {
				$this->view->add_tag("value", $text);
			}
			$this->view->close_tag();
		}

		private function show_information_form($information) {
			if (($business = $this->model->get_business()) === false) {
				$this->view->add_tag("result", "Error fetching business entities.");
				return false;
			}

			if (($cat_labels = $this->model->get_labels()) === false) {
				$this->view->add_tag("result", "Error getting labels.");
				return false;
			}

			if (($help = $this->model->get_help()) === false) {
				$this->view->add_tag("result", "Error fetching help text.");
				return false;
			}

			if (is_array($information["labels"] ?? null) == false) {
				$information["labels"] = array();
			}

			$this->view->add_javascript("cms/information.js");
			$this->view->add_css("includes/labels.css");
			$this->view->add_help_button();

			$this->view->open_tag("edit");

			$this->view->add_tag("help", $help);

			$this->view->record($information, "information");

			$this->view->open_tag("business", array("owner" => $information["owner_type"]));
			$this->view->add_tag("item", "(none)", array("id" => 0));
			foreach ($business as $item) {
				$this->view->add_tag("item", $item["name"], array("id" => $item["id"]));
			}
			$this->view->close_tag();

			$this->add_list("confidentiality", CONFIDENTIALITY);
			$this->add_list("integrity", INTEGRITY);
			$this->add_list("availability", AVAILABILITY);

			$this->view->open_tag("labels");
			foreach ($cat_labels as $category => $labels) {
				$this->view->open_tag("category", array("name" => $category));
				foreach ($labels as $label) {
					$param = array(
						"id"      => $label["id"],
						"checked" => show_boolean(in_array($label["id"], $information["labels"])));
					$this->view->add_tag("label", $label["name"], $param);
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save information") {
					/* Save information
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_information_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create information
						 */
						if (($new_info_id = $this->model->create_information($_POST)) === false) {
							$this->view->add_message("Error creating information.");
							$this->show_information_form($_POST);
						} else {
							$this->user->log_action("information %d created", $new_info_id);
							$this->show_overview();
						}
					} else {
						/* Update information
						 */
						if ($this->model->update_information($_POST) === false) {
							$this->view->add_message("Error updating information.");
							$this->show_information_form($_POST);
						} else {
							$this->user->log_action("information %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete information") {
					/* Delete information
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_information_form($_POST);
					} else if ($this->model->delete_information($_POST["id"]) === false) {
						$this->view->add_message("Error deleting information.");
						$this->show_information_form($_POST);
					} else {
						$this->user->log_action("information %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "search") {
					/* Search
					 */
					$_SESSION["information_search"] = $_POST["search"];
					$this->show_overview();
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) == "new") {
				/* New information
				 */
				$information = array("owner_type" => "existing");
				$this->show_information_form($information);
			} else if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit information
				 */
				if (($information = $this->model->get_information($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Information not found.");
				} else {
					$information["owner_type"] = "existing";
					$this->show_information_form($information);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
