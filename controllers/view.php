<?php
	class view_controller extends Banshee\controller {
		private function show_overview() {
			if (($views = $this->model->get_views()) === false) {
				$this->view->add_tag("result", $this->view->global_text("error_database"));
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("views");
			foreach ($views as $view) {
				$view["locked"] = $this->language->global_text(show_boolean($view["locked"]));
				$this->view->record($view, "view");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_view_form($view) {
			$this->view->add_javascript("view_edit.js");

			$this->view->open_tag("edit");
			$view["locked"] = show_boolean($view["locked"] ?? false);
			$this->view->record($view, "view");
			$this->view->close_tag();
		}

		private function show_build_form($view) {
			if (($applications = $this->model->get_applications($view["id"])) === false) {
				return false;
			}

			if (($business = $this->model->get_business($view["id"])) === false) {
				return false;
			}

			if (($information = $this->model->get_information($view["id"])) === false) {
				return false;
			}

			if (($file_operations = $this->model->get_file_operations($view["id"])) === false) {
				return false;
			}

			if (($hardware = $this->model->get_hardware($view["id"])) === false) {
				return false;
			}

			if (($connections = $this->model->get_connections()) === false) {
				return false;
			}

			if (($apps_run_at = $this->model->get_app_runs_at()) === false) {
				return false;
			}

			if (($apps_used_by = $this->model->get_app_used_by()) === false) {
				return false;
			}

			if (($allocation = $this->model->get_information_allocation()) === false) {
				return false;
			}

			if (($frames = $this->model->get_frames($view["id"])) === false) {
				return false;
			}

			$this->view->title = $view["name"];
			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("webui/jquery.ui.touch-punch.js");
			$this->view->add_javascript("banshee/jquery.windowframe.js");
			$this->view->add_javascript("view.js");

			$this->view->add_css("webui/jquery-ui.css");

			$this->view->open_tag("build");

			$view["locked"] = show_boolean($view["locked"] ?? false);
			$this->view->record($view, "view");

			foreach ($applications as $application) {
				$this->view->record($application, "application");

				if ($application["owner_id"] != null) {
					$this->view->add_tag("link", "", array(
						"type" => "owner",
						"from" => "a".$application["id"],
						"to"   => "b".$application["owner_id"]));
				}
			}

			foreach ($business as $entity) {
				$this->view->record($entity, "business");
			}

			foreach ($information as $info) {
				$this->view->record($info, "information");

				if ($info["owner_id"] != null) {
					$this->view->add_tag("link", "", array(
						"type" => "owner",
						"from" => "i".$info["id"],
						"to"   => "b".$info["owner_id"]));
				}
			}

			foreach ($file_operations as $operation) {
				$this->view->record($operation, "file_operation");

				if ($operation["application_id"] != null) {
					$this->view->add_tag("link", $operation["format"], array(
						"type" => "operation",
						"from" => "f".$operation["id"],
						"to"   => "a".$operation["application_id"],
						"flow" => $operation["data_flow"]));
				}
			}

			foreach ($hardware as $device) {
				$this->view->record($device, "hardware");
			}

			foreach ($connections as $connection) {
				$this->view->add_tag("link", $connection["protocol"], array(
					"type" => "connection",
					"from" => "a".$connection["from_application_id"],
					"to"   => "a".$connection["to_application_id"],
					"flow" => $connection["data_flow"]));
			}

			foreach ($apps_run_at as $app_runs_at) {
				$this->view->add_tag("link", "", array(
					"type" => "runs_at",
					"from" => "a".$app_runs_at["application_id"],
					"to"   => "h".$app_runs_at["hardware_id"]));
			}

			foreach ($apps_used_by as $app_usage) {
				$this->view->add_tag("link", $app_usage["input"], array(
					"type" => "app_usage",
					"from" => "a".$app_usage["application_id"],
					"to"   => "b".$app_usage["business_id"]));
			}

			foreach ($allocation as $alloc) {
				$this->view->add_tag("link", INFORMATION_STORAGE_TYPES[$alloc["type"]], array(
					"type" => "allocation",
					"from" => "a".$alloc["application_id"],
					"to"   => "i".$alloc["information_id"]));
			}

			foreach ($frames as $frame) {
				$this->view->add_tag("frame", $frame["title"], array(
					"id"     => $frame["id"],
					"x"      => $frame["x"],
					"y"      => $frame["y"],
					"width"  => $frame["width"],
					"height" => $frame["height"]));
			}

			$this->view->close_tag();
		}

		private function handle_action($action) {
			switch ($action["action"]) {
				case "element_show":
					$this->model->show_element($action);
					break;
				case "element_hide":
					$this->model->hide_element($action);
					break;
				case "element_move":
					$this->model->move_element($action);
					break;
				case "frame_create":
					if (($frame_id = $this->model->create_frame($action)) == false) {
						$frame_id = -1;
					}
					$this->view->add_tag("frame_id", $frame_id);
					break;
				case "frame_move":
					$this->model->move_frame($action);
					break;
				case "frame_resize":
					$this->model->resize_frame($action);
					break;
				case "frame_rename":
					$this->model->rename_frame($action);
					break;
				case "frame_delete":
					$this->model->delete_frame($action);
					break;
			}
		}

		public function execute() {
			$this->view->title = $this->language->module_text("views");

			if ($this->page->ajax_request) {
				$this->handle_action($_POST);
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("btn_save_view")) {
					/* Save view
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_view_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create view
						 */
						if (($view_id = $this->model->create_view($_POST)) === false) {
							$this->view->add_message("Error creating view.");
							$this->show_view_form($_POST);
						} else {
							$this->user->log_action("view %d created", $view_id);
							if (($view = $this->model->get_view($view_id)) == false) {
								$this->show_overview();
							} else {
								$this->show_build_form($view);
							}
						}
					} else {
						/* Update view
						 */
						if ($this->model->update_view($_POST) === false) {
							$this->view->add_message("Error updating view.");
							$this->show_view_form($_POST);
						} else {
							$this->user->log_action("view %d updated", $_POST["id"]);
							$this->show_build_form($_POST);
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_view")) {
					/* Delete view
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_view_form($_POST);
					} else if ($this->model->delete_view($_POST["id"]) === false) {
						$this->view->add_message("Error deleting view.");
						$this->show_view_form($_POST);
					} else {
						$this->user->log_action("view %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New view
				 */
				$view = array(
					"width" => 3000,
					"height" => 2000);
				$this->show_view_form($view);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit view
				 */
				if (($view = $this->model->get_view($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", "View not found.");
				} else {
					$this->show_view_form($view);
				}
			} else if ($this->page->parameter_value(0, "build") && $this->page->parameter_numeric(1)) {
				if (($view = $this->model->get_view($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", "View not found.");
				} else {
					$this->show_build_form($view);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
