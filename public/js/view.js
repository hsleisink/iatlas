const ARROW_SIZE = 12;
const ARROW_ANGLE = 0.4;
const ARROW_DIST_CONN = 5;
const ARROW_DIST_DATA = 20;
const GRID_SIZE = 15;

const COLOR_LINE = '#808080';
const COLOR_TEXT = '#ff0000';
const COLOR_RUNS_AT = '#208020';
const COLOR_OPERATION = '#e08000';

var view_id = null;
var canvas = null;
var ctx = null;
var dist_h, dist_v;
var corner1, corner2, corner3, corner4;
var ctrl_down = false;

$(document).ready(function() {
	var locked = $('div.view').attr('locked') == 'yes';
	$('div.content div.container').addClass(locked ? 'locked' : 'editable');

	$('div.content div.container').removeClass('container');
	view_id = $('div.view').attr('view_id');

	$('div.content').addClass('content-view');

	var element = $('div.element').first();
	if (element.length > 0) {
		dist_h = element.outerWidth() / 2;
		dist_v = element.outerHeight() / 2
		corner1 = Math.atan2(dist_v, dist_h);
		corner2 = Math.PI - corner1;
		corner3 = Math.PI + corner1;
		corner4 = 2 * Math.PI - corner1;
	}

	/* Initialize canvas
	 */
	var view = $('div.view');
	var width = view.width();
	var height = view.height();
	$('div.view-container').prepend('<canvas id="links" class="links" width="' + width + '" height="' + height + '" />');
	canvas = document.getElementById('links');

	if (ctx == null) {
		ctx = canvas.getContext('2d');
		ctx.lineWidth = 1;

		ctx.font = '12px Arial';
		ctx.textAlign = 'center';
	}

	/* Make elements draggable
	 */
	if (locked == false) {
		var groups = [ 'application', 'business', 'file_operation', 'information', 'hardware' ];
		groups.forEach(function(group) {
			$('div.' + group).draggable({
				containment: 'div.view',
				start: function(event, ui) {
					$(this).appendTo('div.view');
				},
				drag: update_links,
				stop: function(event, ui) {
					var element = ui.helper;
					var pos = element.position();

					$.post('/view', {
						element_id: element.attr('id'),
						view_id: view_id,
						x: Math.round(pos.left),
						y: Math.round(pos.top),
						action: 'element_move'
					}).fail(function() {
						alert('Your session has expired.');
					});

					$(this).draggable({
						grid: false
					});
				}
			});

			$('div.' + group).on('mousedown', function() {
				if (ctrl_down == false) {
					return;
				}

				var pos = $(this).position();
				pos.left = Math.round(pos.left);
				pos.top = Math.round(pos.top);

				pos.left = pos.left - (pos.left % GRID_SIZE);
				pos.top = pos.top - (pos.top % GRID_SIZE);

				$(this).css('left', pos.left + 'px');
				$(this).css('top', pos.top + 'px');

				update_links();

				$(this).draggable({
					grid: [GRID_SIZE, GRID_SIZE]
				});
			});

			$('div.' + group).on('mouseenter', function() {
				$('div.sidebar li').removeClass('selected');
				$('div.sidebar li').removeClass('highlight');

				var item_id = $(this).attr('id');

				$('div.sidebar input[toggles="' + item_id + '"]').parent().addClass('selected');

				$('div.links div[from="' + item_id + '"]').each(function() {
					var target = $(this).attr('to');
					$('div.sidebar input[toggles="' + target + '"]:not(:checked)').parent().addClass('highlight');
				});

				$('div.links div[to="' + item_id + '"]').each(function() {
					var target = $(this).attr('from');
					$('div.sidebar input[toggles="' + target + '"]:not(:checked)').parent().addClass('highlight');
				});
			});
		});

		/* Frames
		 */
		$('div.frame').each(function() {
			frame_activate($(this));
		});
	}

	update_links();

	/* Help
	 */
	$('div.help').windowframe({
		activator: 'button.help',
		header: 'Help',
		top: '50px'
	});

	/* Background hack for printing
	 */
	$('div.element').append('<div class="background-hack"></div>');
	$('div.element div.background-hack').each(function() {
		$(this).css('border-color', $(this).parent().css('background-color'));
	});

	/* Key presses
	 */
	$('body').keydown(function(event) {
		if (event.which == 17) {
			ctrl_down = true;
		}
	});

	$('body').keyup(function(event) {
		if (event.which == 17) {
			ctrl_down = false;
		}
	});
});

function draw_arrow_head(from, to, filled, spacing = 0, color = COLOR_LINE) {
	var dx = from.left - to.left;
	var dy = from.top - to.top;
	var length = Math.sqrt(dx * dx + dy * dy);

	var angle = Math.atan2(dy, dx);
	if (angle < 0) {
		angle += Math.PI * 2;
	}

	ctx.strokeStyle = color;
	ctx.fillStyle = color;

	var dist, side;
	if ((angle < corner1) || ((angle >= corner2) && (angle < corner3)) || (angle >= corner4)) {
		// Left and right
		dist = dist_h;
		side = Math.tan(angle) * dist;
	} else if (((angle >= corner1) && (angle < corner2)) || ((angle >= corner3) && (angle < corner4))) {
		// Top and bottom
		dist = dist_v;
		side = Math.tan(angle - Math.PI / 2) * dist;
	}

	dist = Math.sqrt(side * side + dist * dist) + spacing;
	if (2.5 * dist > length) {
		return;
	}

	var head_x = to.left + Math.cos(angle) * dist;
	var head_y = to.top + Math.sin(angle) * dist;

	var angle_side = (angle + ARROW_ANGLE) % (Math.PI * 2);
	var side_x = head_x + Math.cos(angle_side) * ARROW_SIZE;
	var side_y = head_y + Math.sin(angle_side) * ARROW_SIZE;

	ctx.beginPath();
	ctx.moveTo(side_x, side_y);
	ctx.lineTo(head_x, head_y);

	var angle_side = angle - ARROW_ANGLE;
	if (angle_side < 0) {
		angle_side += Math.PI * 2;
	}
	var side_x = head_x + Math.cos(angle_side) * ARROW_SIZE;
	var side_y = head_y + Math.sin(angle_side) * ARROW_SIZE;

	ctx.lineTo(side_x, side_y);

	ctx.stroke();
	if (filled) {
		ctx.fill();
	}
}

function update_links() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	$('div.links div.link').each(function() {
		/* From point
		 */
		var element_from = $('div#' + $(this).attr('from'));

		if (element_from.length == 0) {
			return true;
		} else if (element_from.is(':visible') == false) {
			return true;
		}

		var from = element_from.position();
		from.left += (element_from.outerWidth() >> 1);
		from.top += (element_from.outerHeight() >> 1);

		/* To point
		 */
		var element_to = $('div#' + $(this).attr('to'));

		if (element_to.length == 0) {
			return true;
		} else if (element_to.is(':visible') == false) {
			return true;
		}

		var to = element_to.position();
		to.left += (element_to.outerWidth() >> 1);
		to.top += (element_to.outerHeight() >> 1);

		/* Draw line
		 */
		ctx.strokeStyle = COLOR_LINE;
		ctx.setLineDash([]);

		switch ($(this).attr('type')) {
			case 'owner':
				ctx.setLineDash([2, 5]);
				break;
			case 'runs_at':
				ctx.strokeStyle = COLOR_RUNS_AT;
				break;
			case 'app_usage':
			case 'data_usage':
				ctx.setLineDash([10, 3]);
				break;
			case 'operation':
				ctx.strokeStyle = COLOR_OPERATION;
				break;
		}

		ctx.beginPath();
		ctx.moveTo(from.left, from.top);
		ctx.lineTo(to.left, to.top);
		ctx.stroke();

		/* Arrow head
		 */
		if ($(this).attr('type') == 'connection') {
			draw_arrow_head(from, to, false, ARROW_DIST_CONN);
			switch (parseInt($(this).attr('flow'))) {
				case 0:
					draw_arrow_head(to, from, true, ARROW_DIST_DATA);
					break;
				case 1:
					draw_arrow_head(from, to, true, ARROW_DIST_DATA);
					break;
				case 2:
					draw_arrow_head(from, to, true, ARROW_DIST_DATA);
					draw_arrow_head(to, from, true, ARROW_DIST_DATA);
					break;
			}
		} else if ($(this).attr('type') == 'app_usage') {
			if ($(this).text() != '') {
				draw_arrow_head(to, from, true, ARROW_DIST_DATA);
			}
		} else if ($(this).attr('type') == 'operation') {
			switch (parseInt($(this).attr('flow'))) {
				case 0:
					draw_arrow_head(from, to, true, ARROW_DIST_DATA, COLOR_OPERATION);
					break;
				case 1:
					draw_arrow_head(to, from, true, ARROW_DIST_DATA, COLOR_OPERATION);
					break;
				case 2:
					draw_arrow_head(from, to, true, ARROW_DIST_DATA, COLOR_OPERATION);
					draw_arrow_head(to, from, true, ARROW_DIST_DATA, COLOR_OPERATION);
					break;
			}
		}

		var text = $(this).text();
		if (text != '') {
			ctx.fillStyle = COLOR_TEXT;
			ctx.fillText(text, (from.left + to.left) >> 1, (from.top + to.top + 8) >> 1);
		}
	});
}

function list_toggle(type) {
	$('div.sidebar li.' + type).toggle(100);
}

function element_toggle(element_id) {
	var element = $('div#' + element_id);

	var x = Math.round($('div.view-container').width() >> 1) +
	        Math.round($('div.view-container').scrollLeft()) - 50;
	var y = Math.round($('div.view-container').height() >> 1) +
	        Math.round($('div.view-container').scrollTop()) - 25;

	if (element.is(':visible') == false) {
		/* Show element
		 */
		$.post('/view', {
			view_id: view_id,
			element_id: element_id,
			x: x,
			y: y,
			action: 'element_show'
		}).done(function() {
			element.show();
			element.css('left', x + 'px');
			element.css('top', y + 'px');
			update_links();
		}).fail(function() {
			alert('Your session has expired.');
		});
	} else {
		/* Hide element
		 */
		$.post('/view', {
			view_id: view_id,
			element_id: element_id,
			action: 'element_hide'
		}).done(function() {
			element.hide();
			update_links();
		}).fail(function() {
			alert('Your session has expired.');
		});
	}
}

/* Frame functions
 */
function frame_activate(frame) {
	$('div.frame span.frame-title').css('cursor', 'pointer');
	$('div.frame span.frame-delete').css('display', 'inline-block');
	
	frame.draggable({
		containment: 'div.view',
		handle: 'span.frame-title',
		stop: function(event, ui) {
			var frame = ui.helper;
			var pos = frame.position();

			$.post('/view', {
				frame_id: frame.attr('frame_id'),
				view_id: view_id,
				x: Math.round(pos.left),
				y: Math.round(pos.top),
				action: 'frame_move'
			}).fail(function() {
				alert('Your session has expired.');
			});
		}
	});

	frame.resizable({
		stop: function(event, ui) {
			var frame = ui.helper;

			$.post('/view', {
				frame_id: frame.attr('frame_id'),
				view_id: view_id,
				width: Math.round(frame.width()),
				height: Math.round(frame.height()),
				action: 'frame_resize'
			}).fail(function() {
				alert('Your session has expired.');
			});
		}
	});

	frame.find('span.frame-title').on('dblclick', function() {
		var frame = $(this).parent().parent();
		var title = prompt('New frame\'s title:', $(this).text());
		if (title == null) {
			return;
		}

		$.post('/view', {
			frame_id: frame.attr('frame_id'),
			view_id: view_id,
			title: title,
			action: 'frame_rename'
		}).done(function() {
			frame.find('span.frame-title').text(title);
		}).fail(function() {
			alert('Your session has expired.');
		});
	});

	frame.find('span.frame-delete').on('click', function() {
		if (confirm('Delete this frame?')) {
			var frame = $(this).parent().parent();

			$.post('/view', {
				frame_id: frame.attr('frame_id'),
				view_id: view_id,
				action: 'frame_delete'
			}).done(function() {
				frame.remove();
			}).fail(function() {
				alert('Your session has expired.');
			});
		}
	});
}

function frame_add() {
	var title = prompt('New frame\'s title:');
	if (title == null) {
		return;
	}

	var width = 300;
	var height = 100;
	var left = (Math.round($('div.view-container').width()) / 2) - (width / 2);
	var top = (Math.round($('div.view-container').height()) / 2) - (height / 2);

	$.post('/view', {
		view_id: view_id,
		title: title,
		x: left,
		y: top,
		width: width,
		height: height,
		action: 'frame_create'
	}).done(function(data) {
		var frame_id = $(data).find('frame_id').text();
		if ((frame_id == undefined) || (frame_id == -1)) {
			alert('Error while creating frame.');
			return;
		}

		$('div.view').prepend('<div frame_id="' + frame_id + '" class="frame"><div class="frame-header"><span class="frame-title">' + title + '</span><span class="frame-delete">&#215;</span></div>');
		var frame = $('div.view div[frame_id="' + frame_id + '"]');

		frame.css('left', left + 'px');
		frame.css('top', top + 'px');
		frame.css('width', width + 'px');
		frame.css('height', height + 'px');

		frame_activate(frame);
	}).fail(function() {
		alert('Your session has expired.');
	});
}
