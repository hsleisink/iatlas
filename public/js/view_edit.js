$(document).ready(function() {
	var sizes = {
		'A3 landscape': {
			width:  1477,
			height:  952
		},
		'A3 portrait': {
			width:  1011,
			height: 1417
		},
		'A4 landscape': {
			width:  1012,
			height:  622
		},
		'A4 portrait': {
			width:   684,
			height:  952
		},
		'US letter': {
			width:   705,
			height:  885
		}
	};

	var table = $('table.sizes tbody');
	for (const [size, value] of Object.entries(sizes)) {
		table.append('<tr><td>' + size + '</td><td>' + value.width + '</td><td>' + value.height + '</td></tr>');
	}

	table.find('tr').on('click', function() {
		$('input#width').val($(this).find('td:nth-child(2)').text());
		$('input#height').val($(this).find('td:nth-child(3)').text());
	});
});
