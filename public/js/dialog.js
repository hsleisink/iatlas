var dialogs = new Object();

$(document).ready(function() {
	$('div.dialogs div').each(function() {
		var id = $(this).attr('id');
		var title = $(this).attr('title');

		dialogs[id] = $('div#' + id).windowframe({
			header: title
    	});
	});
});

function show_dialog(type, id) {
	dialogs['des_' + type + '_' + id].open();
}
