<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:if test="info_without_owner/information">
<h2>Information without owner</h2>
<table class="table table-striped table-condensed no_owner">
<thead>
<tr><th>Name</th></tr>
</thead>
<tbody>
<xsl:for-each select="info_without_owner/information">
<tr>
<td><a href="/cms/information/{@id}"><xsl:value-of select="name" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="storage_issues/information">
<h2>Information main storage issues</h2>
<table class="table table-striped table-condensed no_hdw">
<thead>
<tr><th>Name</th><th>Number of main storage locations</th></tr>
</thead>
<tbody>
<xsl:for-each select="storage_issues/information">
<tr>
<td><a href="/cms/application/link/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="main_count" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="apps_without_owner/application">
<h2>Applications without owner</h2>
<table class="table table-striped table-condensed no_owner">
<thead>
<tr><th>Name</th></tr>
</thead>
<tbody>
<xsl:for-each select="apps_without_owner/application">
<tr>
<td><a href="/cms/application/{@id}"><xsl:value-of select="name" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="apps_without_hardware/application">
<h2>Applications without hardware</h2>
<table class="table table-striped table-condensed no_hdw">
<thead>
<tr><th>Name</th></tr>
</thead>
<tbody>
<xsl:for-each select="apps_without_hardware/application">
<tr>
<td><a href="/cms/application/link/{@id}"><xsl:value-of select="name" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="crowded/device">
<h2>Crowded servers</h2>
<table class="table table-striped table-condensed crowded">
<thead>
<tr><th>Name</th><th>Applications</th></tr>
</thead>
<tbody>
<xsl:for-each select="crowded/device">
<tr>
<td><a href="/cms/application/link"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="applications" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="isolated_business/business">
<h2>Isolated business entities</h2>
<table class="table table-striped table-condensed iso_bus">
<thead>
<tr><th>Name</th></tr>
</thead>
<tbody>
<xsl:for-each select="isolated_business/business">
<tr>
<td><a href="/cms/application/link"><xsl:value-of select="name" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="isolated_hardware/device">
<h2>Isolated hardware devices</h2>
<table class="table table-striped table-condensed iso_hdw">
<thead>
<tr><th>Name</th></tr>
</thead>
<tbody>
<xsl:for-each select="isolated_hardware/device">
<tr>
<td><a href="/cms/application/link"><xsl:value-of select="name" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="privacy_issues/processing">
<h2>Privacy issues</h2>
<table class="table table-striped table-condensed privacy">
<thead>
<tr><th>Application</th><th>Confidentiality</th><th>Processing</th></tr>
</thead>
<tbody>
<xsl:for-each select="privacy_issues/processing">
<tr>
<td><a href="/application/{app_id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="confidentiality" /></td>
<td><a href="/processing/{@id}"><xsl:value-of select="processing" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<div class="btn-group">
<a href="/cms" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/issues.png" class="title_icon" />
<h1>Issues</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
