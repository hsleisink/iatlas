<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}" method="post">
<label for="application_id">Application:</label>
<select id="application_id" name="application_id" class="form-control" onChange="javascript:submit()">
<xsl:for-each select="applications/application">
<option value="{@id}"><xsl:if test="@id=../@selected"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>
<input type="hidden" name="submit_button" value="set application" />
</form>

<h2>Information</h2>
<table class="table table-hover table-striped table-condensed table-xs">
<thead class="table-xs">
<tr><th>Information</th><th>Owner</th><th>Type</th></tr>
</thead>
<tbody>
<xsl:for-each select="allocation/information">
<tr onClick="javascript:document.location='/{/output/page}/information/{@id}'">
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="owner" /></td>
<td><xsl:value-of select="type" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2>Connections</h2>
<table class="table table-hover table-striped table-condensed table-xs">
<thead class="table-xs">
<tr><th>From</th><th>To</th><th>Protocol</th><th>Format</th><th>Frequency</th><th>Data flow</th></tr>
</thead>
<tbody>
<xsl:for-each select="connections/connection">
<tr onClick="javascript:document.location='/{/output/page}/connection/{@id}'">
<td><span class="table-xs">From:</span><xsl:value-of select="from_name" /></td>
<td><span class="table-xs">To:</span><xsl:value-of select="to_name" /></td>
<td><span class="table-xs">Protocol:</span><xsl:value-of select="protocol" /></td>
<td><span class="table-xs">Format:</span><xsl:value-of select="format" /></td>
<td><span class="table-xs">Frequency:</span><xsl:value-of select="frequency" /></td>
<td><span class="table-xs">Data flow:</span><xsl:value-of select="data_flow" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2>File operations</h2>
<table class="table table-hover table-striped table-condensed table-xs">
<thead class="table-xs">
<tr><th>Location</th><th>Format</th><th>Frequency</th><th>Data flow</th></tr>
</thead>
<tbody>
<xsl:for-each select="file_operations/file_operation">
<tr onClick="javascript:document.location='/{/output/page}/fileoperation/{@id}'">
<td><span class="table-xs">Location:</span><xsl:value-of select="location" /></td>
<td><span class="table-xs">Format:</span><xsl:value-of select="format" /></td>
<td><span class="table-xs">Frequency:</span><xsl:value-of select="frequency" /></td>
<td><span class="table-xs">Data flow:</span><xsl:value-of select="data_flow" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="row">
<div class="col-sm-6">

<h2>Used by</h2>
<table class="table table-hover table-striped table-condensed">
<thead>
<tr><th>Name</th><th>Information input</th></tr>
</thead>
<tbody>
<xsl:for-each select="used_by/entity">
<tr onClick="javascript:document.location='/{/output/page}/usedby/{@id}'">
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="input" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

</div>
<div class="col-sm-6">

<h2>Runs at</h2>
<table class="table table-hover table-striped table-condensed">
<thead>
<tr><th>Name</th><th>Operating System</th></tr>
</thead>
<tbody>
<xsl:for-each select="runs_at/device">
<tr onClick="javascript:document.location='/{/output/page}/runsat/{@id}'">
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="os" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

</div>
</div>

<div class="btn-group btn-responsive">
<a href="/{/output/page}/information" class="btn btn-default">New information allocation</a>
<a href="/{/output/page}/connection" class="btn btn-default">New connection</a>
<a href="/{/output/page}/fileoperation" class="btn btn-default">New file operation</a>
<a href="/{/output/page}/usedby" class="btn btn-default">New used-by</a>
<a href="/{/output/page}/runsat" class="btn btn-default">New runs-at</a>
<a href="/cms/application" class="btn btn-default">Back</a>
</div>

<div id="help">
<h2>Information</h2>
<p>Here you specify what information is stored at this server and wheather it's the main storage location of that information or that the information is just a copy of the actual information.</p>

<h2>Connections</h2>
<p>Applications often exchange information automatically with other application.</p>
<ul>
<li><b>From:</b> The device that initiates the connection.</li>
<li><b>To:</b> The device to which the connection is made.</li>
<li><b>Protocol:</b> The application layer protocol used in the connection.</li>
<li><b>Format:</b> Optional. The format of files transfered withing the connection.</li>
<li><b>Freqency:</b> The frequency of new connections.</li>
<li><b>Data flow:</b> The direction of the data flow from the From-device's point of view.</li>
</ul>

<h2>File operations</h2>
<p>Some applications read/write data from/to a file, which may be located on another server.</p>
<ul>
<li><b>Location:</b> Path to the file.</li>
<li><b>Format:</b> The format of the file.</li>
<li><b>Freqency:</b> The frequency of the file operation.</li>
<li><b>Data flow:</b> The direction of the data during the file operation.</li>
</ul>

<h2>Used by</h2>
<p>Here you specify who uses this application and if they enter new information (starting point of any information flow within the landscape.</p>

<h2>Runs at</h2>
<p>Here you specify at which server the application is hosted.</p>
</div>
</xsl:template>

<!--
//
//  Information allocation template
//
//-->
<xsl:template match="allocation">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="@id"><input type="hidden" name="id" value="{@id}" /></xsl:if>
<input type="hidden" name="application_id" value="{application_id}" />
<label>From application:</label>
<p><xsl:value-of select="application" /></p>

<label>Information:</label>
<select id="information_id" name="information_id" class="form-control">
<xsl:for-each select="information/information">
<option value="{@id}"><xsl:if test="@id=../../information_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>

<label>Storage type:</label>
<select id="type" name="type" class="form-control">
<xsl:for-each select="storage/type">
<option value="{@id}"><xsl:if test="@id=../../type"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save allocation" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="@id">
<input type="submit" name="submit_button" value="Delete allocation" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Connection template
//
//-->
<xsl:template match="connection">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="@id"><input type="hidden" name="id" value="{@id}" /></xsl:if>
<input type="hidden" name="from_application_id" value="{from_application_id}" />
<label>From application:</label>
<p><xsl:value-of select="application" /></p>
<label for="to_application_id">To application:</label>
<select id="to_application_id" name="to_application_id" class="form-control">
<xsl:for-each select="applications/application">
<option value="{@id}"><xsl:if test="@id=../../to_application_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>
<label for="protocol">Protocol (include version number and encryption):</label>
<input type="text" id="protocol" name="protocol" value="{protocol}" class="form-control" />
<label for="format">Format:</label>
<input type="text" id="format" name="format" value="{format}" class="form-control" />
<label for="frequency">Frequency:</label>
<input type="text" id="frequency" name="frequency" value="{frequency}" class="form-control" />
<label for="data_flow">Data flow:</label>
<select id="data_flow" name="data_flow" class="form-control">
<xsl:for-each select="data_flow/direction">
<option value="{@id}"><xsl:if test="@id=../../data_flow"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="description">Description:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="description" /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save connection" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="@id">
<input type="submit" name="submit_button" value="Delete connection" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>

<div id="help">
<ul>
<li><b>From application:</b> The application that initiates the connection.</li>
<li><b>To application:</b> The application to which the connection is made.</li>
<li><b>Protocol:</b> The protocol that is used to transport the information, e.g. HTTP</li>
<li><b>Format:</b> The format in which the information is transported, e.g. XML.</li>
<li><b>Frequency:</b> How often the information is being send.</li>
<li><b>Data flow:</b> The direction in which the information flows.</li>
</ul>
</div>
</xsl:template>

<!--
//
//  File operation template
//
//-->
<xsl:template match="file_operation">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="@id"><input type="hidden" name="id" value="{@id}" /></xsl:if>
<input type="hidden" name="application_id" value="{application_id}" />
<label>From application:</label>
<p><xsl:value-of select="application" /></p>
<label for="location">Location:</label>
<input type="text" id="location" name="location" value="{location}" class="form-control" />
<label for="format">Format:</label>
<input type="text" id="format" name="format" value="{format}" class="form-control" />
<label for="frequency">Frequency:</label>
<input type="text" id="frequency" name="frequency" value="{frequency}" class="form-control" />
<label for="data_flow">Data flow:</label>
<select id="data_flow" name="data_flow" class="form-control">
<xsl:for-each select="data_flow/direction">
<option value="{@id}"><xsl:if test="@id=../../data_flow"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="description">Description:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="description" /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save file operation" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="@id">
<input type="submit" name="submit_button" value="Delete file operation" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Used by template
//
//-->
<xsl:template match="usedby">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="@id"><input type="hidden" name="id" value="{@id}" /></xsl:if>
<input type="hidden" name="application_id" value="{application_id}" />
<label>Application:</label>
<p><xsl:value-of select="application" /></p>
<label for="business_id">Used by:</label>
<select id="business_id" name="business_id" class="form-control">
<xsl:for-each select="business/entity">
<option value="{@id}"><xsl:if test="@id=../../business_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>
<label for="input">Information input:</label>
<input type="text" id="input" name="input" value="{input}" class="form-control" />
<label for="description">Description:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="description" /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save used-by" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="@id">
<input type="submit" name="submit_button" value="Delete used-by" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>

<div id="help">
<ul>
<li><b>Information input:</b> Description of manually entered information.</li>
</ul>
</div>
</xsl:template>

<!--
//
//  Runs at template
//
//-->
<xsl:template match="runsat">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="@id"><input type="hidden" name="id" value="{@id}" /></xsl:if>
<input type="hidden" name="application_id" value="{application_id}" />
<label>Application:</label>
<p><xsl:value-of select="application" /></p>
<label for="hardware_id">Runs at:</label>
<select id="hardware_id" name="hardware_id" class="form-control">
<xsl:for-each select="hardware/device">
<option value="{@id}"><xsl:if test="@id=../../hardware_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save runs-at" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="@id">
<input type="submit" name="submit_button" value="Delete runs-at" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/application.png" class="title_icon" />
<h1>Application link administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="allocation" />
<xsl:apply-templates select="connection" />
<xsl:apply-templates select="file_operation" />
<xsl:apply-templates select="usedby" />
<xsl:apply-templates select="runsat" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
