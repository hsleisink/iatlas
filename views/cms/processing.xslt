<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />
<xsl:import href="../banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}" method="post" class="search">
<div class="input-group">
<input type="text" id="search" name="search" value="{@search}" class="form-control" placeholder="Search" />
<span class="input-group-btn">
<input type="button" class="btn btn-default" value="x" onClick="javascript:$('input#search').val(''); submit();" />
</span>
</div>
<input type="hidden" name="submit_button" value="search" />
</form>

<table class="table table-condensed table-striped table-hover overview">
<thead>
<tr><th>Description</th><th>Role</th></tr>
</thead>
<tbody>
<xsl:for-each select="processings/processing">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="description" /></td>
<td><xsl:value-of select="role" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New processing</a>
<a href="/cms" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="processing/@id">
<input type="hidden" name="id" value="{processing/@id}" />
</xsl:if>

<label for="information">Information:</label>
<select id="information" name="information_id" class="form-control">
<xsl:for-each select="information/information">
<option value="{@id}"><xsl:if test="@id=../../processing/information_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="description">Description:</label>
<input id="description" name="description" value="{processing/description}" class="form-control" />
<label for="role">Role:</label>
<select id="role" name="role" class="form-control">
<xsl:if test="not(processing/@id)"><xsl:attribute name="onChange">javascript:role_change()</xsl:attribute></xsl:if>
<xsl:if test="processing/@id"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if>
<xsl:for-each select="roles/role">
<option value="{position()-1}"><xsl:if test="position()-1=../../processing/role"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<div class="controller">
<label for="contact">Contact information of the controller:</label>
</div>
<div class="processor">
<label for="contact">Contact information of the processor and responsible controller:</label>
</div>
<textarea id="contact" name="contact" class="form-control"><xsl:value-of select="processing/contact" /></textarea>
<div class="controller">
<label for="purpose">Purpose of the processing:</label>
</div>
<div class="processor">
<label for="purpose">Categories of the processing carried out on behalf of controller:</label>
</div>
<textarea id="purpose" name="purpose" class="form-control"><xsl:value-of select="processing/purpose" /></textarea>
<div class="controller">
<label for="subject">Categories of the data subjects and personal data:</label>
<textarea id="subject" name="subject" class="form-control"><xsl:value-of select="processing/subject" /></textarea>
<label for="recipient">Categories of recipients:</label>
<textarea id="recipient" name="recipient" class="form-control"><xsl:value-of select="processing/recipient" /></textarea>
</div>
<label for="transfer">Data transfer to third country or international organisation:</label>
<textarea id="transfer" name="transfer" class="form-control"><xsl:value-of select="processing/transfer" /></textarea>
<div class="controller">
<label for="erasure">Envisaged time limits for data erasure:</label>
<textarea id="erasure" name="erasure" class="form-control"><xsl:value-of select="processing/erasure" /></textarea>
</div>
<label for="security">Technical and organisational security measures:</label>
<textarea id="security" name="security" class="form-control"><xsl:value-of select="processing/security" /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save processing" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="processing/@id">
<input type="submit" name="submit_button" value="Delete processing" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/processing.png" class="title_icon" />
<h1>Processing activity administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
