<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-striped table-condensed overview">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/global/owner" /></th>
<th><xsl:value-of select="/output/language/global/security_need" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="information">
<tr>
<td><a href="/{/output/page}/{@id}"><xsl:value-of select="name" /></a></td>
<td><a href="/business/{owner_id}"><xsl:value-of select="owner" /></a></td>
<td><xsl:value-of select="value" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/overview" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Information template
//
//-->
<xsl:template match="information">
<xsl:if test="description!=''"><div class="panel panel-default panel-body"><xsl:value-of select="description" /></div></xsl:if>

<div class="row classification">
<div class="col-sm-12"><xsl:value-of select="/output/language/global/owner" />: <a href="/business/{owner_id}"><xsl:value-of select="owner" /></a></div>
</div>

<div class="row classification">
<div class="col-sm-4"><xsl:value-of select="/output/language/global/confidentiality" />: <xsl:value-of select="confidentiality" /></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/global/integrity" />: <xsl:value-of select="integrity" /></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/global/availability" />: <xsl:value-of select="availability" /></div>
</div>

<xsl:if test="labels/label">
<div class="labels"><xsl:value-of select="/output/language/global/labels" />:<xsl:for-each select="labels/label">
<xsl:if test="position()>1"><xsl:if test="@cid!=preceding::*[1]/@cid"><span>,</span></xsl:if></xsl:if>
<a href="/label/{@id}" class="label label-primary"><xsl:value-of select="." /></a>
</xsl:for-each></div>
</xsl:if>

<xsl:if test="count(applications/application)>0">
<h2><xsl:value-of select="/output/language/global/applications" /></h2>
<table class="table table-striped table-condensed">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/global/storage_type" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="applications/application">
<tr>
<td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="type" /></td>
<td><xsl:if test="description!=''"><span class="glyphicon glyphicon-info-sign" onClick="javascript:show_dialog('appuse', {@id});" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="count(processings/processing)>0">
<table class="table table-striped table-condensed">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/processing" /></th>
<th><xsl:value-of select="/output/language/module/role" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="processings/processing">
<tr>
<td><a href="/processing/{@id}"><xsl:value-of select="description" /></a></td>
<td><xsl:value-of select="role" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<div class="btn-group">
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
<a href="/information" class="btn btn-default"><xsl:value-of select="/output/language/module/all_information" /></a>
</div>

<div class="dialogs">
<xsl:for-each select="used_by/entity">
<xsl:if test="description!=''">
<div id="des_use_{@id}" title="Used by {name}"><span><xsl:value-of select="description" /></span></div>
</xsl:if>
</xsl:for-each>
</div>

<div id="help">
<ul>
<xsl:value-of select="/output/language/global/help_classification" disable-output-escaping="yes" />
</ul>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/information.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="information" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
