<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Protocols template
//
//-->
<xsl:template match="protocols">
<table class="table table-condensed table-striped protocols">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/protocol" /></th>
<th><xsl:value-of select="/output/language/module/from_application" /></th>
<th><xsl:value-of select="/output/language/module/to_application" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="record">
<tr>
<td><xsl:value-of select="protocol" /><xsl:if test="format!=''"> / <xsl:value-of select="format" /></xsl:if></td>
<td><a href="/application/{from_application_id}"><xsl:value-of select="from_app" /></a></td>
<td><a href="/application/{to_application_id}"><xsl:value-of select="to_app" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  Input template
//
//-->
<xsl:template match="input">
<table class="table table-condensed table-striped input">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/input" /></th>
<th><xsl:value-of select="/output/language/module/at_application" /></th>
<th><xsl:value-of select="/output/language/module/by_business" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="record">
<tr>
<td><xsl:value-of select="input" /></td>
<td><a href="/application/{app_id}"><xsl:value-of select="application" /></a></td>
<td><a href="/business/{bus_id}"><xsl:value-of select="business" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  Security template
//
//-->
<xsl:template match="security">
<table class="table table-condensed table-striped value">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/application" /></th>
<th><xsl:value-of select="/output/language/global/confidentiality" /></th>
<th><xsl:value-of select="/output/language/global/integrity" /></th>
<th><xsl:value-of select="/output/language/global/availability" /></th>
<th><xsl:value-of select="/output/language/global/security_need" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="record">
<tr>
<td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="confidentiality" /></td>
<td><xsl:value-of select="integrity" /></td>
<td><xsl:value-of select="availability" /></td>
<td><xsl:value-of select="value" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  File operations template
//
//-->
<xsl:template match="files">
<table class="table table-condensed table-striped">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/application" /></th>
<th><xsl:value-of select="/output/language/global/location" /></th>
<th><xsl:value-of select="/output/language/global/format" /></th>
<th><xsl:value-of select="/output/language/global/frequency" /></th>
<th><xsl:value-of select="/output/language/global/data_flow" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="record">
<tr>
<td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="location" /></td>
<td><xsl:value-of select="format" /></td>
<td><xsl:value-of select="frequency" /></td>
<td><xsl:value-of select="data_flow" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  Internet facing template
//
//-->
<xsl:template match="internet">
<table class="table table-condensed table-striped value">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/application" /></th>
<th><xsl:value-of select="/output/language/global/confidentiality" /></th>
<th><xsl:value-of select="/output/language/global/integrity" /></th>
<th><xsl:value-of select="/output/language/global/availability" /></th>
<th><xsl:value-of select="/output/language/global/security_need" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="record">
<tr>
<td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="confidentiality" /></td>
<td><xsl:value-of select="integrity" /></td>
<td><xsl:value-of select="availability" /></td>
<td><xsl:value-of select="value" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  List template
//
//-->
<xsl:template match="list">
<form action="/{/output/page}" method="post" class="types" onChange="javascript:submit()">
<select name="type" class="form-control">
<xsl:for-each select="types/option">
<option value="{@type}"><xsl:if test="@type=../@selected"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</form>

<xsl:apply-templates select="protocols" />
<xsl:apply-templates select="input" />
<xsl:apply-templates select="security" />
<xsl:apply-templates select="files" />
<xsl:apply-templates select="internet" />
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/list.png" class="title_icon" />
<h1><xsl:value-of select="/output/language/module/lists" /></h1>
<xsl:apply-templates select="list" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
