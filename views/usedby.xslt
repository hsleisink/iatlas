<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Used-by template
//
//-->
<xsl:template match="usedby">
<dl>
	<dt><xsl:value-of select="/output/language/global/application" />:</dt>
	<dd><a href="/application/{application_id}"><xsl:value-of select="application" /></a></dd>
	<dt><xsl:value-of select="/output/language/global/used_by" />:</dt>
	<dd><a href="/business/{business_id}"><xsl:value-of select="business" /></a></dd>
	<dt><xsl:value-of select="/output/language/global/information_input" />:</dt>
	<dd><xsl:value-of select="input" /></dd>
	<dt><xsl:value-of select="/output/language/global/description" />:</dt>
	<dd class="description"><xsl:value-of select="description" /></dd>
</dl>

<div class="btn-group">
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/global/used_by" /></h1>
<xsl:apply-templates select="usedby" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
