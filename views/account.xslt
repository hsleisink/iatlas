<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<label for="fullname"><xsl:value-of select="/output/language/module/name" />:</label>
<input type="text" id="fullname" name="fullname" value="{fullname}" class="form-control" />
<label for="email"><xsl:value-of select="/output/language/module/email_address" />:</label>
<input type="text" id="email" name="email" value="{email}" class="form-control" />
<label><xsl:value-of select="/output/language/module/organisation" />:</label>
<input type="text" disabled="disabled" value="{organisation}" class="form-control" />
<label for="current"><xsl:value-of select="/output/language/module/password_current" />:</label>
<input type="password" id="current" name="current" class="form-control" />
<label for="password"><xsl:value-of select="/output/language/module/password_new" />:</label> <span class="blank" style="font-size:10px">(<xsl:value-of select="/output/language/module/not_changed_when_blank" />)</span>
<input type="password" id="password" name="password" class="form-control" />
<label for="repeat"><xsl:value-of select="/output/language/module/password_repeat" />:</label>
<input type="password" id="repeat" name="repeat" class="form-control" />
<xsl:if test="@authenticator='yes'">
<label for="secret"><xsl:value-of select="/output/language/module/authenticator_secret" />:</label> [<span class="info" onClick="javascript:$('#as_dialog').dialog()">?</span>]
<div class="input-group">
	<input type="text" id="secret" name="authenticator_secret" value="{authenticator_secret}" class="form-control" style="text-transform:uppercase" />
	<span class="input-group-btn"><input type="button" value="Generate" class="btn btn-default" onClick="javascript:set_authenticator_code()" /></span>
</div>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_account_update}" class="btn btn-default" />
<a href="/{@logout}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_logout" /></a>
</div>
<xsl:if test="/output/user/@admin='no'">
<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_account_delete}" class="btn btn-danger" onClick="javascript:return prompt('This account and all of its data will be deleted. Type \'DELETE\' when you are sure.') == 'DELETE';" />
</div>
</xsl:if>
</form>

<h2><xsl:value-of select="/output/language/module/recent_account_activity" /></h2>
<table class="table table-striped table-xs">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/ip_address" /></th>
<th><xsl:value-of select="/output/language/module/timestamp" /></th>
<th><xsl:value-of select="/output/language/module/activity" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="actionlog/log">
<tr>
<td><xsl:value-of select="ip" /></td>
<td><xsl:value-of select="timestamp" /></td>
<td><xsl:value-of select="message" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div id="as_dialog" title="Authenticator app">
<xsl:value-of select="/output/language/module/authenticator_help" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/account.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
