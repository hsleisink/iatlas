<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Connection template
//
//-->
<xsl:template match="connection">
<dl>
	<dt><xsl:value-of select="/output/language/global/from" />:</dt>
	<dd><a href="/application/{from_application_id}"><xsl:value-of select="from_name" /></a></dd>
	<dt><xsl:value-of select="/output/language/global/to" />:</dt>
	<dd><a href="/application/{to_application_id}"><xsl:value-of select="to_name" /></a></dd>
	<dt><xsl:value-of select="/output/language/global/protocol" />:</dt>
	<dd><xsl:value-of select="protocol" /></dd>
	<dt><xsl:value-of select="/output/language/global/format" />:</dt>
	<dd><xsl:value-of select="format" /></dd>
	<dt><xsl:value-of select="/output/language/global/frequency" />:</dt>
	<dd><xsl:value-of select="frequency" /></dd>
	<dt><xsl:value-of select="/output/language/global/data_flow" />:</dt>
	<dd><xsl:value-of select="data_flow" /></dd>
	<dt><xsl:value-of select="/output/language/global/description" />:</dt>
	<dd class="description"><xsl:value-of select="description" /></dd>
</dl>

<div class="btn-group">
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/connection" /></h1>
<xsl:apply-templates select="connection" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
