<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://gitlab.com/hsleisink/banshee/
	 *
	 * Licensed under The MIT License
	 */

	namespace Banshee\Core;

	class language {
		const GLOBAL_MODULE = "*";

		private $db = null;
		private $page = null;
		private $view = null;
		private $global_texts = null;
		private $global_replacements = array();
		private $module_texts = null;
		private $module_replacements = array();
		private $supported = null;

		/* Constructor
		 *
		 * INPUT:  object database, object page, object view
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($db, $page, $view) {
			$this->db = $db;
			$this->page = $page;
			$this->view = $view;

			$this->supported = config_array(SUPPORTED_LANGUAGES);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "select language") {
					$this->view->set_language($_POST["language"]);

					$_SERVER["REQUEST_METHOD"] = "GET";
					$_POST = array();
				}
			}
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			switch ($key) {
				case "supported": return $this->supported;
			}

			return null;
		}

		/* Reset loaded module texts
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function reset() {
			$this->module_texts = null;
			$this->module_replacements = array();
		}

		/* Load texts from database
		 *
		 * INPUT:  string page
		 * OUTPUT: array texts
		 * ERROR:  -
		 */
		private function load_texts($page) {
			$result = array();

			$query = "select name,javascript,%S as content from languages where module=%s";
			if (($messages = $this->db->execute($query, $this->view->language, $page)) != false) {
				foreach ($messages as $message) {
					$result[$message["name"]] = array(
						"javascript" => show_boolean($message["javascript"]),
						"text"       => $message["content"]);
				}
			}

			return $result;
		}

		/* Get global text
		 *
		 * INPUT:  string text name
		 * OUTPUT: string text
		 * ERROR:  false
		 */
		public function global_text($name) {
			if ($this->global_texts === null) {
				$this->global_texts = $this->load_texts(self::GLOBAL_MODULE);
			}

			if (isset($this->global_texts[$name])) {
				return $this->global_texts[$name]["text"];
			}

			return "GT{".$name."}";
		}

		/* Get module text
		 *
		 * INPUT:  string text name
		 * OUTPUT: string text
		 * ERROR:  null
		 */
		public function module_text($name) {
			if ($this->module_texts === null) {
				$this->module_texts = $this->load_texts($this->page->module);
			}

			if (isset($this->module_texts[$name])) {
				return $this->module_texts[$name]["text"];
			}

			return "MT{".$name."}";
		}

		/* Replace text in global text
		 *
		 * INPUT:  string name, string key, string value
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function replace_global_text($name, $key, $value) {
			if (isset($this->global_replacements[$name]) == false) {
				$this->global_replacements[$name] = array();
			}

			$this->global_replacements[$name][$key] = $value;
		}

		/* Replace text in module text
		 *
		 * INPUT:  string name, string key, string value
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function replace_module_text($name, $key, $value) {
			if (isset($this->module_replacements[$name]) == false) {
				$this->module_replacements[$name] = array();
			}

			$this->module_replacements[$name][$key] = $value;
		}

		/* Add all texts for page to XML output
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function add_to_view() {
			if ($this->global_texts === null) {
				$this->global_texts = $this->load_texts(self::GLOBAL_MODULE);
			}

			if ($this->module_texts === null) {
				$this->module_texts = $this->load_texts($this->page->page);
			}

			$this->view->open_tag("language", array("code" => $this->view->language));

			$this->view->open_tag("languages");
			foreach (config_array(SUPPORTED_LANGUAGES) as $code => $language) {
				$this->view->add_tag("language", $language, array("code" => $code));
			}
			$this->view->close_tag();

			$this->view->open_tag("global");
			foreach ($this->global_texts as $name => $content) {
				if (isset($this->global_replacements[$name])) {
					foreach ($this->global_replacements[$name] as $key => $value) {
						$content["text"] = str_replace("{{".$key."}}", $value, $content["text"]);
					}
				}
				$this->view->add_tag($name, $content["text"], array("javascript" => $content["javascript"]));
			}
			$this->view->close_tag();

			$this->view->open_tag("module");
			foreach ($this->module_texts as $name => $content) {
				if (isset($this->module_replacements[$name])) {
					foreach ($this->module_replacements[$name] as $key => $value) {
						$content["text"] = str_replace("{{".$key."}}", $value, $content["text"]);
					}
				}

				$this->view->add_tag($name, $content["text"], array("javascript" => $content["javascript"]));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}
	}
?>
