iAtlas
======

iAtlas is a tool to help you create insight in all the information within your
organisation. It is loosly based on ArchiMate, but it uses a more high level
approach. It has its own internal view creation module to create print-outs.
