<?php
	class fileoperation_model extends Banshee\model {
		public function get_file_operation($id) {
			$query = "select f.*, ".
			         "(select name from applications where id=f.application_id) as application ".
			         "from file_operations f, applications a where f.id=%d and f.application_id=a.id and a.organisation_id=%d ".
			         "order by application";

			if (($result = $this->db->execute($query, $id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

	}
?>
