<?php
	class cms_issues_model extends Banshee\model {
		public function get_info_without_owner() {
			$query = "select * from information where owner_id is null and organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_storage_issues() {
			$query = "select id, name, (select count(*) from information_application a ".
			           "where i.id=a.information_id and a.type=%d) as main_count ".
			         "from information i where organisation_id=%d having main_count!=1";

			return $this->db->execute($query, 0, $this->user->organisation_id);
		}

		public function get_apps_without_owner() {
			$query = "select * from applications where owner_id is null and organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_apps_without_hardware() {
			$query = "select *, (select count(*) from application_hardware where application_id=a.id) as hardware ".
			         "from applications a having hardware=%d and location=%d and organisation_id=%d";

			return $this->db->execute($query, 0, 0, $this->user->organisation_id);
		}

		public function get_crowded_servers() {
			$query = "select *, (select count(*) from application_hardware where hardware_id=h.id) as applications ".
			         "from hardware h having applications>%d and organisation_id=%d order by applications desc";

			return $this->db->execute($query, $this->settings->max_apps_per_server, $this->user->organisation_id);
		}

		public function get_isolated_business() {
			$query = "select *, (select count(*) from application_business where business_id=b.id) as usedby, ".
			         "(select count(*) from applications where owner_id=b.id) as owning ".
			         "from business b where b.organisation_id=%d having usedby=%d and owning=%d";

			return $this->db->execute($query, $this->user->organisation_id, 0, 0);
		}

		public function get_isolated_hardware() {
			$query = "select *, (select count(*) from application_hardware where hardware_id=h.id) as runsat ".
			         "from hardware h where h.organisation_id=%d having runsat=%d";

			return $this->db->execute($query, $this->user->organisation_id, 0);
		}

		public function get_privacy_issues() {
			$query = "select i.name, i.id as app_id, i.confidentiality, p.id, p.description as processing ".
			         "from processings p, information i where p.information_id=i.id ".
			         "and i.organisation_id=%d and i.confidentiality<%d order by name";

			if (($result = $this->db->execute($query, $this->user->organisation_id, 1)) === false) {
				return false;
			}

			foreach ($result as $i => $item) {
				$result[$i]["confidentiality"] = CONFIDENTIALITY[$item["confidentiality"]];
			}

			return $result;
		}
	}
?>
