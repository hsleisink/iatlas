<?php
	class cms_application_link_model extends Banshee\model {
		/* Application
		 */
		public function get_applications() {
			$query = "select * from applications where organisation_id=%d order by name";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_application($application_id) {
			$query = "select a.*, b.name as owner ".
			         "from applications a left join business b on a.owner_id=b.id ".
			         "where a.id=%d and a.organisation_id=%d";

			if (($result = $this->db->execute($query, $application_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function valid_application_id($application_id) {
			$query = "select count(*) as count from applications where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $application_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return (int)$result[0]["count"] > 0;
		}

		/* Business
		 */
		public function get_business() {
			$query = "select * from business where organisation_id=%d order by name";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		private function get_entity($business_id) {
			$query = "select * from business where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $business_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		/* Hardware
		 */
		public function get_hardware() {
			$query = "select * from hardware where organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		private function get_device($hardware_id) {
			$query = "select * from hardware where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $hardware_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		/* Information allocation
		 */
		public function get_allocation_list($application_id) {
			$query = "select l.id, i.name, b.name as owner, l.type from information_application l, information i ".
			         "left join business b on i.owner_id=b.id ".
			         "where i.id=l.information_id and l.application_id=%d order by name";

			return $this->db->execute($query, $application_id);
		}

		public function get_allocation($allocation_id) {
			$query = "select l.* from information i, information_application l ".
			         "where i.organisation_id=%d and i.id=l.information_id and l.id=%d";
			
			if (($result = $this->db->execute($query, $this->user->organisation_id, $allocation_id)) === false) {
				return false;
			}

			return $result[0];
		}

		public function get_information() {
			$query = "select * from information where organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function allocation_oke($allocation) {
			$query = "select count(*) as count from information_application where information_id=%d and application_id=%d";
			$args = array($allocation["information_id"], $allocation["application_id"]);
			if (isset($allocation["id"])) {
				$query .= " and id!=%d";
				array_push($args, $allocation["id"]);
			}
			if (($result = $this->db->execute($query, $args)) === false) {
				$this->view->add_message("Database error.");
				return false;
			}
			if ($result[0]["count"] > 0) {
				$this->view->add_message("This application is already hosting this information.");
				return false;
			}

			return true;
		}

		public function save_allocation($allocation) {
			$keys = array("information_id", "application_id", "type");

			if (isset($allocation["id"]) == false) {
				array_unshift($keys, "id");
				$allocation["id"] = null;

				return $this->db->insert("information_application", $allocation, $keys) !== false;
			} else {
				return $this->db->update("information_application", $allocation["id"], $allocation, $keys) !== false;
			}
		}

		public function delete_allocation($allocation_id) {
			if ($this->get_allocation($allocation_id) == false) {
				$this->user->log_action("unauthorized delete attempt of allocation %d", $allocation_id);
				return false;
			}

			return $this->db->delete("information_application", $allocation_id);
		}

		/* Connection
		 */
		public function get_connection_list($application_id) {
			$query = "select *, ".
			         "(select name from applications where id=c.from_application_id and organisation_id=%d) as from_name, ".
			         "(select name from applications where id=c.to_application_id and organisation_id=%d) as to_name ".
			         "from connections c where from_application_id=%d or to_application_id=%d order by from_name, to_name";

			return $this->db->execute($query, $this->user->organisation_id, $this->user->organisation_id, $application_id, $application_id);
		}

		public function get_connection($connection_id) {
			$query = "select c.* from connections c, applications a ".
			         "where c.from_application_id=a.id and c.id=%d and a.organisation_id=%d";

			if (($result = $this->db->execute($query, $connection_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function connection_oke($connection) {
			if (isset($connection["id"])) {
				if ($this->get_connection($connection["id"]) == false) {
					$this->view->add_message("Connection not found.");
					$this->user->log_action("unauthorized update attempt of connection %d", $connection["id"]);
					return false;
				}
			}

			if ($this->get_application($connection["from_application_id"]) == false) {
				$this->view->add_message("Unknown from-application.");
				return false;
			}

			if ($this->get_application($connection["to_application_id"]) == false) {
				$this->view->add_message("Unknown to-application.");
				return false;
			}

			return true;
		}

		public function save_connection($connection) {
			$keys = array("from_application_id", "to_application_id", "protocol", "format", "frequency", "data_flow", "description");

			if (isset($connection["id"]) == false) {
				array_unshift($keys, "id");
				$connection["id"] = null;

				return $this->db->insert("connections", $connection, $keys) !== false;
			} else {
				return $this->db->update("connections", $connection["id"], $connection, $keys) !== false;
			}
		}

		public function delete_connection($connection_id) {
			if ($this->get_connection($connection_id) == false) {
				$this->user->log_action("unauthorized delete attempt of connection %d", $connection_id);
				return false;
			}

			return $this->db->delete("connections", $connection_id);
		}

		/* File operation
		 */
		public function get_file_operation_list($application_id) {
			$query = "select *, ".
			         "(select name from applications where id=f.application_id and organisation_id=%d) as application ".
			         "from file_operations f where application_id=%d order by application";

			return $this->db->execute($query, $this->user->organisation_id, $application_id);
		}

		public function get_file_operation($file_operation_id) {
			$query = "select f.* from file_operations f, applications a ".
			         "where f.application_id=a.id and f.id=%d and a.organisation_id=%d";

			if (($result = $this->db->execute($query, $file_operation_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function file_operation_oke($file_operation) {
			if (isset($file_operation["id"])) {
				if ($this->get_file_operation($file_operation["id"]) == false) {
					$this->view->add_message("File operation not found.");
					$this->user->log_action("unauthorized update attempt of file operation %d", $file_operation["id"]);
					return false;
				}
			}

			if (trim($file_operation["location"]) == "") {	
				$this->view->add_message("Specify the location.");
				return false;
			}

			if ($this->get_application($file_operation["application_id"]) == false) {
				$this->view->add_message("Unknown application.");
				return false;
			}

			return true;
		}

		public function save_file_operation($file_operation) {
			$keys = array("application_id", "location", "format", "frequency", "data_flow", "description");

			if (isset($file_operation["id"]) == false) {
				array_unshift($keys, "id");
				$file_operation["id"] = null;

				return $this->db->insert("file_operations", $file_operation, $keys) !== false;
			} else {
				return $this->db->update("file_operations", $file_operation["id"], $file_operation, $keys) !== false;
			}
		}

		public function delete_file_operation($file_operation_id) {
			if ($this->get_file_operation($file_operation_id) == false) {
				$this->user->log_action("unauthorized delete attempt of file operation %d", $file_operation_id);
				return false;
			}

			return $this->db->delete("file_operations", $file_operation_id);
		}

		/* Used by
		 */
		public function get_usedby_list($application_id) {
			$query = "select u.*, b.name from business b, application_business u ".
			         "where b.id=u.business_id and u.application_id=%d and b.organisation_id=%d ".
			         "order by name";

			return $this->db->execute($query, $application_id, $this->user->organisation_id);
		}

		public function get_usedby($usedby_id) {
			$query = "select u.* from application_business u, applications a ".
			         "where u.application_id=a.id and u.id=%d and a.organisation_id=%d";

			if (($result = $this->db->execute($query, $usedby_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function usedby_oke($usedby) {
			if (isset($usedby["id"])) {
				if ($this->get_usedby($usedby["id"]) == false) {
					$this->view->add_message("Used-by not found.");
					$this->user->log_action("unauthorized update attempt of app-used-by %d", $usedby["id"]);
					return false;
				}
			}

			if ($this->get_application($usedby["application_id"]) == false) {
				$this->view->add_message("Unknown application.");
				return false;
			}

			if ($this->get_entity($usedby["business_id"]) == false) {
				$this->view->add_message("Unknown business entity.");
				return false;
			}

			$query = "select count(*) as count from application_business where application_id=%d and business_id=%d";
			$args = array($usedby["application_id"], $usedby["business_id"]);
			if (isset($usedby["id"])) {
				$query .= " and id!=%d";
				array_push($args, $usedby["id"]);
			}
			if (($result = $this->db->execute($query, $args)) === false) {
				$this->view->add_message("Database error.");
				return false;
			}
			if ($result[0]["count"] > 0) {
				$this->view->add_message("This business entity already uses this application.");
				return false;
			}

			return true;
		}

		public function save_usedby($usedby) {
			$keys = array("application_id", "business_id", "input", "description");

			if (isset($usedby["id"]) == false) {
				array_unshift($keys, "id");
				$usedby["id"] = null;

				return $this->db->insert("application_business", $usedby, $keys) !== false;
			} else {
				return $this->db->update("application_business", $usedby["id"], $usedby, $keys) !== false;
			}
		}

		public function delete_usedby($usedby_id) {
			if ($this->get_usedby($usedby_id) == false) {
				$this->user->log_action("unauthorized delete attempt of app-used-by %d", $userby_id);
				return false;
			}

			return $this->db->delete("application_business", $usedby_id);
		}

		/* Runs at
		 */
		public function get_runsat_list($application_id) {
			$query = "select r.*, h.name, h.os from hardware h, application_hardware r ".
			         "where h.id=r.hardware_id and r.application_id=%d and h.organisation_id=%d ".
			         "order by name";

			return $this->db->execute($query, $application_id, $this->user->organisation_id);
		}

		public function get_runsat($usedby_id) {
			$query = "select r.* from application_hardware r, applications a ".
			         "where r.application_id=a.id and r.id=%d and a.organisation_id=%d";

			if (($result = $this->db->execute($query, $usedby_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function runsat_oke($runsat) {
			if (isset($runsat["id"])) {
				if ($this->get_runsat($runsat["id"]) == false) {
					$this->view->add_message("Runs-at not found.");
					$this->user->log_action("unauthorized update attempt of runs-at %d", $runsat["id"]);
					return false;
				}
			}

			if ($this->get_application($runsat["application_id"]) == false) {
				$this->view->add_message("Unknown application.");
				return false;
			}

			if ($this->get_device($runsat["hardware_id"]) == false) {
				$this->view->add_message("Unknown device.");
				return false;
			}

			$query = "select count(*) as count from application_hardware where application_id=%d and hardware_id=%d";
			$args = array($runsat["application_id"], $runsat["hardware_id"]);
			if (isset($runsat["id"])) {
				$query .= " and id!=%d";
				array_push($args, $runsat["id"]);
			}
			if (($result = $this->db->execute($query, $args)) === false) {
				$this->view->add_message("Database error.");
				return false;
			}
			if ($result[0]["count"] > 0) {
				$this->view->add_message("This application already runs at the assigned server.");
				return false;
			}

			return true;
		}

		public function save_runsat($runsat) {
			$keys = array("application_id", "hardware_id");

			if (isset($runsat["id"]) == false) {
				array_unshift($keys, "id");
				$runsat["id"] = null;

				return $this->db->insert("application_hardware", $runsat, $keys) !== false;
			} else {
				return $this->db->update("application_hardware", $runsat["id"], $runsat, $keys) !== false;
			}
		}

		public function delete_runsat($runsat_id) {
			if ($this->get_runsat($runsat_id) == false) {
				$this->user->log_action("unauthorized delete attempt of runs-at %d", $runsat_id);
				return false;
			}

			return $this->db->delete("application_hardware", $runsat_id);
		}
	}
?>
