<?php
	class information_model extends Banshee\model {
		public function get_all_information() {
			$query = "select i.*, b.name as owner from information i ".
			         "left join business b on i.owner_id=b.id ".
			         "where i.organisation_id=%d order by i.name";

			if (($information = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			foreach ($information as $idx => $info) {
				$i = $info["integrity"];
				$c = $info["confidentiality"];
				$a = $info["availability"];

				$information[$idx]["value"] = ASSET_VALUE_LABELS[ASSET_VALUE[$c][$i][$a]];
			}

			return $information;
		}

		public function get_information($information_id) {
			$query = "select i.*, b.name as owner ".
			         "from information i left join business b on i.owner_id=b.id ".
			         "where i.id=%d and i.organisation_id=%d";

			if (($result = $this->db->execute($query, $information_id, $this->user->organisation_id)) == false) {
				return false;
			}

			$information = $result[0];

			$query = "select l.* from labels l, label_information i ".
			         "where l.id=i.label_id and i.information_id=%d order by name";
			$information["labels"] = $this->db->execute($query, $information_id);

			return $information;
		}

		public function get_applications($information_id) {
			$query = "select a.id, a.name, l.type from applications a, information_application l ".
			         "where a.id=l.application_id and l.information_id=%d and organisation_id=%d";

			return $this->db->execute($query, $information_id, $this->user->organisation_id);
		}

		public function get_processings($information_id) {
			$query = "select p.* from processings p, information i ".
			         "where p.information_id=i.id and  p.information_id=%d and i.organisation_id=%d";

			return $this->db->execute($query, $information_id, $this->user->organisation_id);
		}
	}
?>
