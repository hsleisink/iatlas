<?php
	class list_model extends Banshee\model {
		public function get_protocols() {
			$query = "select c.*, ".
			         "(select name from applications where id=c.from_application_id) as from_app, ".
			         "(select name from applications where id=c.to_application_id) as to_app ".
			         "from connections c, applications a where c.from_application_id=a.id and a.organisation_id=%d ".
			         "order by protocol, format, from_app, to_app";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_input() {
			$query = "select l.input, b.id as bus_id, b.name as business, ".
			         "a.id as app_id, a.name as application ".
			         "from application_business l, business b, applications a ".
			         "where b.id=l.business_id and l.application_id=a.id and ".
			         "input!=%s and a.organisation_id=%d order by input";

			return $this->db->execute($query, "", $this->user->organisation_id);
		}

		private function value_sort($app_a, $app_b) {
			if ($app_a["value"] == $app_b["value"]) {
				$a = ($app_a["confidentiality"] + 1)^2 * ($app_a["integrity"] + 1)^2 * ($app_a["availability"] + 1)^2;
				$b = ($app_b["confidentiality"] + 1)^2 * ($app_b["integrity"] + 1)^2 * ($app_b["availability"] + 1)^2;

				if ($a == $b ) {
					return strcmp($app_a["name"], $app_b["name"]);
				} else {
					return $a < $b ? 1 : -1;
				}
			} else {
				return $app_a["value"] < $app_b["value"] ? 1 : -1;
			}
		}

		private function get_information_value($applications) {
			foreach ($applications as $idx => $application) {
				$i = $application["integrity"];
				$c = $application["confidentiality"];
				$a = $application["availability"];

				$applications[$idx]["value"] = ASSET_VALUE[$i][$c][$a] ?? 0;
			}

			usort($applications, array($this, "value_sort"));

			foreach ($applications as $idx => $application) {
				$applications[$idx]["confidentiality"] = CONFIDENTIALITY[$application["confidentiality"]] ?? "";
				$applications[$idx]["integrity"] = INTEGRITY[$application["integrity"]] ?? "";
				$applications[$idx]["availability"] = AVAILABILITY[$application["availability"]] ?? "";
				$applications[$idx]["value"] = ASSET_VALUE_LABELS[$application["value"]];
			}

			return $applications;
		}

		public function get_security() {
			$query = "select *, ".
			         "(select max(confidentiality) from information i, information_application l ".
			         "where i.id=l.information_id and l.application_id=a.id) as confidentiality, ".
			         "(select max(integrity) from information i, information_application l ".
			         "where i.id=l.information_id and l.application_id=a.id) as integrity, ".
			         "(select max(availability) from information i, information_application l ".
			         "where i.id=l.information_id and l.application_id=a.id) as availability ".
			         "from applications a where organisation_id=%d";
			if (($applications = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			return $this->get_information_value($applications);
		}

		public function get_files() {
			$query = "select a.id, a.name, f.location, f.format, f.frequency, f.data_flow ".
			         "from file_operations f, applications a ".
			         "where f.application_id=a.id and a.organisation_id=%d";

			if (($result = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			foreach ($result as $i => $item) {
				$result[$i]["data_flow"] = FILE_DATA_FLOW[$item["data_flow"]];
			}

			return $result;
		}

		public function get_internet() {
			$query = "select *, ".
			         "(select max(confidentiality) from information i, information_application l ".
			         "where i.id=l.information_id and l.application_id=a.id) as confidentiality, ".
			         "(select max(integrity) from information i, information_application l ".
			         "where i.id=l.information_id and l.application_id=a.id) as integrity, ".
			         "(select max(availability) from information i, information_application l ".
			         "where i.id=l.information_id and l.application_id=a.id) as availability ".
			         "from applications a where organisation_id=%d and location<%d and internet=%d";

			if (($applications = $this->db->execute($query, $this->user->organisation_id, 2, YES)) === false) {
				return false;
			}

			return $this->get_information_value($applications);
		}
	}
?>
