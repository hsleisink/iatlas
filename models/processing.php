<?php
	class processing_model extends Banshee\model {
		public function get_processings() {
			$query = "select p.*, i.name, i.id as info_id from processings p, information i ".
			         "where p.information_id=i.id and i.organisation_id=%d order by i.name";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_processing($processing_id) {
			$query = "select p.*, i.id as info_id, i.name from processings p, information i ".
			         "where p.information_id=i.id and i.organisation_id=%d and p.id=%d";

			if (($result = $this->db->execute($query, $this->user->organisation_id, $processing_id)) == false) {
				return false;
			}
			$processing = $result[0];

			$query = "select a.*, b.name as owner, l.type from applications a, information_application l, business b ".
			         "where a.id=l.application_id and a.owner_id=b.id and l.information_id=%d";
			if (($processing["applications"] = $this->db->execute($query, $processing["info_id"])) === false) {
				return false;
			}

			return $processing;
		}
	}
?>
